provider "" {
}

module "test" {
}

resource "local_file" "inventory_file" {
  content = templatefile("./inventory.tpl", { test-servers = join("\n", formatlist("%s ansible_host=%s", module.test.test_names, module.test.test_ips)) })
  filename = "./inventory"
  file_permission = "0644"
}

