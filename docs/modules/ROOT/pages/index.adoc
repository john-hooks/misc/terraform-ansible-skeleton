= Overview

https://gitlab.com/hooksie1/terraform-ansible-skeleton[Terraform Ansible Skeleton^] is a skeleton project containing the bare minimum Jenkinsfile, Terraform configs, and ansible information to deploy infrastructure.


== Makefile
There is a simple Makefile that controls the whole project. To deploy the infrastructure you can just run `make inventory -j env=environment type=infrastructure-type`.

The environment is the directory to deploy from (dev is the existing) and type is the directory of the type of infrastructure (service is the existing)


The Makefile runs the Terraform commands in parallel with the `make roles` target that will download any specified roles into the `ansible/roles` directory. First the roles directory is cleaned to ensure a clean copy of the roles is downloaded. Then it downloads the roles using `ansible-galaxy`. 


== Needed Information

You will need to supply the environment variables for your authentication in the Jenkinsfile. 

You will also need to create the ansible playbooks and roles needed for Ansible to deploy the configurations.
