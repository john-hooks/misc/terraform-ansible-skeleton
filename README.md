# Terraform and Ansible Skeleton

This is a skeleton project for using Terraform and Ansible

Documentation with examples [here](https://docs.hooks.technology/terraform-ansible-skeleton/index.html)
