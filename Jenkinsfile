pipeline {
    parameters {
        choice(
            choices: ['dev','prod'],
            description: 'Environment to deploy to',
            name: 'Environment'
        )
        choice(
            choices: ['services'],
            description: 'Type of infrastructure to deploy',
            name: 'Type'
        )
    }
    options {
        buildDiscarder(logRotator(numToKeepStr: '10'))
        timeout(time:1, unit: 'HOURS')
    }
    environment {
        ANSIBLE_HOST_KEY_CHECKING = 'False'
        API_ACCESS_KEY = credentials('api-access-key')
        API_SECRET_KEY = credentials('api-secret-key')

    }
    stages {
        stage('Build Infrastructure') {
            steps {
                sh 'make inventory -j env=$Environment type=$Type'
            }
        }
        stage('Run playbook') {
            steps {
                  ansiblePlaybook(
                    playbook: 'ansible/site.yml',
                    inventory: 'ansible/inventory',
                    credentialsId: 'ansible',
                    become: true
                  )
            }
        }
    }
    post {
        success {
            cleanWs()
        }
    }
}
